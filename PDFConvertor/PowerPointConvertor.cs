﻿using Microsoft.Office.Interop.PowerPoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDFConvertor
{
    public class PowerPointConvertor
    {
        public void Convert(string sourcePath, string destPath)
        {
            Microsoft.Office.Interop.PowerPoint.Application ppApp = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = ppApp.Presentations.Open(sourcePath, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoFalse, Microsoft.Office.Core.MsoTriState.msoFalse);
            try
            {
                presentation.ExportAsFixedFormat(destPath,
                    PpFixedFormatType.ppFixedFormatTypePDF,
                    PpFixedFormatIntent.ppFixedFormatIntentPrint,
                    Microsoft.Office.Core.MsoTriState.msoFalse,
                    PpPrintHandoutOrder.ppPrintHandoutHorizontalFirst,
                    PpPrintOutputType.ppPrintOutputSlides,
                    Microsoft.Office.Core.MsoTriState.msoFalse,
                    null,
                    PpPrintRangeType.ppPrintAll,
                    "",
                    false,
                    false,
                    false,
                    true,
                    true,
                    System.Reflection.Missing.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                presentation.Close();
                presentation = null;
                ppApp = null;
                GC.Collect();
            }
        }
    }
}
