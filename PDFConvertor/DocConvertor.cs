﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PDFConvertor
{
    public class DocConvertor
    {
        /// <summary>
        /// Convert the source file to pdf format.
        /// ***Source file will not be deleted.
        /// </summary>
        /// <param name="filePath"> Source. </param>
        /// <param name="destPath"> Destination. </param>
        /// <param name="errorMsg"> Error message. </param>
        /// <returns> Result. </returns>
        public bool Convert(string filePath, string destPath, out string errorMsg)
        {
            bool result = false;
            errorMsg = string.Empty;
            Logger logger = LogManager.GetCurrentClassLogger();
            try
            {
                #region Verify file source path and destination path.
                if (!File.Exists(filePath)) { throw new IOException("Source file does not exist"); }
                if (File.Exists(destPath)) { throw new IOException("Destination file already exists."); }
                if (Path.GetExtension(destPath) != ".pdf") throw new Exception("Can only convert to PDF format.");
                if (!Directory.Exists(Path.GetDirectoryName(destPath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(destPath));
                }
                #endregion

                string extendName = Path.GetExtension(filePath).ToLower();
                switch (extendName)
                {
                    case ".jpg":
                    case ".png":
                    case ".tiff":
                    case ".jpeg":
                    case ".bmp":
                    case ".tif":
                    case ".gif":
                        new ImageConvertor().Convert(filePath, destPath);
                        break;
                    case ".txt":
                    case ".doc":
                    case ".docx":
                        new WordConvertor().Convert(filePath, destPath);                        
                        break;
                    case ".ppt":
                    case ".pptx":
                        new PowerPointConvertor().Convert(filePath, destPath);
                        break;
                    case ".pdf":
                        result = true;
                        File.Copy(filePath, destPath);
                        break;
                    case ".xls":
                    case ".xlsx":
                    case ".csv":
                        new ExcelConvertor().Convert(filePath, destPath);
                        break;
                    default:
                        throw new Exception("this format is not supported.");
                }
                result = File.Exists(destPath);
            }
            catch (Exception ex)
            {
                logger.Error("File = " + filePath);
                logger.Error(ex.Message);
                errorMsg = ex.Message;
                result = false;
            }
            return result;
        }
    }
}
