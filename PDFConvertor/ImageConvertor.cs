﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDFConvertor
{
    class ImageConvertor
    {
        public void Convert(string sourceFile, string destFile)
        {
            PdfDocument doc = new PdfDocument();
            doc.Pages.Add(new PdfPage());
            XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
            XImage img = XImage.FromFile(sourceFile);

            xgr.DrawImage(img, 0, 0);
            doc.Save(destFile);
            doc.Close();
            img = null;
        }
    }
}
