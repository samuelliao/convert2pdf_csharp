﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConvertorTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string source = string.Empty;
            string output = string.Empty;
            while (true)
            {
                Console.WriteLine("Enter \"exit\" to close the program");
                Console.WriteLine("Source file path:");
                source = Console.ReadLine();
                if (source.ToLower() == "exit") break;
                Console.WriteLine("Output file path:");
                output = Console.ReadLine();
                if (output.ToLower() == "exit") break;
                string msg = string.Empty;
                bool result = new PDFConvertor.DocConvertor().Convert(source, output, out msg);
                if (result)
                {
                    Console.WriteLine("Succeed.");
                }
                else
                {
                    Console.WriteLine("Failed");
                    Console.WriteLine(msg);
                }
            }
        }
    }
}
